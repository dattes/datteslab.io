 ---
title: New DATTES Software Paper
date: 2023-11-20
comments: true
---

Our software paper has been accepted for publication in SoftwareX (Elsevier B.V.).

This paper describes DATTES' general features and workflow at the current release (DATTES 23.05).

Of course, this paper is freely available, published as open-access under Creative Commons License (CC-BY 4.0).

If you use DATTES in your work, please cite our paper :

> Eduardo Redondo-Iglesias, Marwan Hassini, Pascal Venet and Serge Pelissier,
DATTES: Data analysis tools for tests on energy storage,
SoftwareX,
Volume 24,
2023,
101584,
ISSN 2352-7110,
https://doi.org/10.1016/j.softx.2023.101584.
(https://www.sciencedirect.com/science/article/pii/S2352711023002807)