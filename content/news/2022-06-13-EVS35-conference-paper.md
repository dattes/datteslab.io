---
title: EVS35-New conference paper !
date: 2022-06-13
---

A new paper using DATTES have been published for EVS35 conference.

The article is entitled " Second Life Batteries in a Mobile Charging Station: Model Based Performance Assessment". Authors are Marwan Hassini, Eduardo Redondo-Iglesias , Pascal Venet, Sylvain Gillet and Younes Zitouni.

The publication is available here : https://hal.archives-ouvertes.fr/hal-03708744
The experimental data are available here : https://data.univ-gustave-eiffel.fr/dataverse/second_life_batteries
And the datapaper describing this dataset is available here : https://hal.archives-ouvertes.fr/hal-03713844

DATTES release used for this article : DATTES 22.06


