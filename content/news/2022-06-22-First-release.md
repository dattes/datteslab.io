 ---
title: DATTES 22.06 Release!
date: 2022-06-22
---

The first official release of DATTES have been released on 2022-06-22 : [Link](https://gitlab.com/dattes/dattes/-/releases)

## Release highlights:

- Full migration from old project named RPT (Reference Performance Tests)

### Code review:

- English translation (variable and function names, help in functions)
- Coding style (snake_case)
- Added documentation on each file (MATLAB help comments)
- Result restructuring on single structure (one single result file)

### Tests on following environments:
- Linux (ubuntu 20.04) + MATLAB (R2021a)
- Linux (ubuntu 20.04) + GNU Octave (5.2.0)
- Windows 10 + MATLAB (R2021b)
- Windows 10 + GNU Octave (7.1.0)


## Known Issues:

` import_arbin_xls` 
- Do not work on Octave (Linux or Windows)
- Partially work on Windows+MATLAB (problem with date/time conversion)
- Fully work on Linux+MATLAB


` import_arbin_res` 
- Not tested on Windows (MATLAB or Octave)
- Needs mdbtools installation
- Fully work on Linux+MATLAB and Linux+Octave


` import_biologic, import_bitrode` 
- Do not work on Windows + Octave : Problem with regexp and backslash file separators
- Fully work on Linux+MATLAB, Linux+Octave and Windows+MATLAB
