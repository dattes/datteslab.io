---
title: Citation
subtitle: How to cite DATTES ?
comments: false
---
## Citing DATTES
If you use DATTES in your work, please cite our paper :

> Eduardo Redondo-Iglesias, Marwan Hassini, Pascal Venet and Serge Pelissier,
DATTES: Data analysis tools for tests on energy storage,
SoftwareX,
Volume 24,
2023,
101584,
ISSN 2352-7110,
https://doi.org/10.1016/j.softx.2023.101584.
(https://www.sciencedirect.com/science/article/pii/S2352711023002807)

You can use the bibtex : 
```
@article{redondo_iglesias_eduardo_2023_8134473,
  title = {DATTES: Data analysis tools for tests on energy storage},
journal = {SoftwareX},
volume = {24},
pages = {101584},
year = {2023},
issn = {2352-7110},
doi = {https://doi.org/10.1016/j.softx.2023.101584},
url = {https://www.sciencedirect.com/science/article/pii/S2352711023002807},
author = {Eduardo Redondo-Iglesias and Marwan Hassini and Pascal Venet and Serge Pelissier},
keywords = {Energy storage, Experiments, Matlab, GNU octave, Data analysis, Open science, FAIR}
}
```