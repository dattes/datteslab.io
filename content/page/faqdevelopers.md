---
title : FAQ developers
subtitle: Frequently Asked Questions - Developing DATTES
comments: false
---

# Outline
- [How to ask a question ?](#ask-a-question)
- [Frequently Asked Questions from developers](#frequently-asked-questions-from-developers)
     - [What is the DATTES license ?](#license-dattes)
     - [How to cite DATTES ?](#citing-dattes)
     - [Why DATTES have been developped in MATLAB/OCTAVE ?](#dattes-language)        

# Ask a question
For any question regarding the development of DATTES, you can email dattes@univ-eiffel.fr.


# Frequently Asked Questions from developers
## DATTES license
DATTES is a free software, licensed under [GNU GPL v3](https://www.gnu.org/licenses/quick-guide)


```
Copyright (c) 2015, E. Redondo, M. Hassini.

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version. This
program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.
You should have received a copy of the GNU General Public
License along with this program. If not, see
<https://www.gnu.org/licenses/>. 
```

## Citing DATTES 

If you use DATTES in your work, please cite this paper

> Redondo-Iglesias, E. (2017). Étude du vieillissement des batteries lithium-ion dans les ap>

You can use the bibtex

```
@phdthesis{Redondo2017etude,
  title={{\'E}tude du vieillissement des batteries lithium-ion dans les applications" v{\'e}>
  author={Redondo Iglesias, Eduardo},
  year={2017},
  school={Universit{\'e} de Lyon}
}
```


We would be grateful if you could also cite some of ours relevant papers.

The complete list of our publications is available [here](https://cv.archives-ouvertes.fr/redondo)

## DATTES language
DATTES is written in MATLAB for two reasons.
First, initially DATTES have been designed to provide experimental results over batteries for the MATLAB/SIMULINK simulation software [VEHLIB](https://gitlab.univ-eiffel.fr/eco7/vehlib).
Second, despite growing interest for python language. MATLAB remains a popular software in the battery field.

As DATTES developers care about open software, all the DATTES code have been designed to enable its use with the open and free software [GNU Octave](https://www.gnu.org/software/octave/index).


