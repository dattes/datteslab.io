---
title: Contribute
subtitle: How to contribute to DATTES ?
comments: false
---

There are several ways to help DATTES development:

- [**Use DATTES**](#using-dattes)
- [**Share DATTES**](#sharing-dattes)
- [**Cite DATTES**](#citing-dattes)
- [**Documente DATTES**](#documenting-dattes)
- [**Develop DATTES**](#developing-dattes)


## Using DATTES

We decided to open DATTES to everyone because we are convinced that it can really help other people
to get the maximum of their experiments' data.

DATTES is the result of working on data from lots of battery experiments on different cyclers (Arbin, Bitrode, Biologic)
from 2015 to now.

Currently DATTES works on linux (all features on MATLAB, almost all features on GNU Octave),
it also works on Windows (mainly MATLAB, GNU Octave needs some progress).

We hope that if you use DATTES on *your* system with *your* data you will get interesting results fast.
At least, with no need to redevelop the wheel.

If you encounter problems when using DATTES (cycler compatibility, feature not working on your system, etc.),
please ask for help!

Open an **issue on gitlab**, explain your problem and we all can search for a solution.

If opening an issue is still too intimidating for you,
you can also ask for help at the following **e-mail** address: [dattes@univ-eiffel.fr](mailto:dattes@univ-eiffel.fr)

## Sharing DATTES
We ❤️ DATTES.
If you love DATTES too, make others to know about it!

- Give a star to the gitlab repository
- Share this website on your favorite social network
- Talk about DATTES at your coffee time
- Share DATTES code (or a part)

DATTES is **FOSS** (Free Open Source Software), and it is licensed under GNU GPL v3, you can then freely:
- **use** it
- **inspect** it
- **modify** it
- **share** it

## Citing DATTES
If you use DATTES in your work, please cite our paper :

> Redondo-Iglesias, Eduardo, Hassini, Marwan, Venet, Pascal, & Pelissier, Serge. (2023). DATTES: Data Analysis Tools for Tests on Energy Storage. Preprint: https://doi.org/10.5281/zenodo.8134473

You can use the bibtex : 
```
@article{redondo_iglesias_eduardo_2023_8134473,
  author       = {Redondo-Iglesias, Eduardo and
                  Hassini, Marwan and
                  Venet, Pascal and
                  Pelissier, Serge},
  title        = {{DATTES: Data Analysis Tools for Tests on Energy 
                   Storage}},
  month        = jul,
  year         = 2023,
  publisher    = {Preprint},
  doi          = {10.5281/zenodo.8134473},
  url          = {https://doi.org/10.5281/zenodo.8134473}
}
```

## Documenting DATTES
There are three possible contibutions for documentation:
- **Documentation pages** on this website:
    - if you can improve these pages please e-mail us [dattes@univ-eiffel.fr](mailto:dattes@univ-eiffel.fr)
- **Examples and use cases**:
    - if you are using DATTES and you have some useful examples, they are also welcome
- **MATLAB help**:
    - if you find MATLAB help could be improved, check the following section to contribute in the same way as a **coder**

## Developing DATTES
If your are a developer, please check [code contribution guidelines](https://gitlab.com/dattes/dattes/-/blob/main/CONTRIBUTING.md)

