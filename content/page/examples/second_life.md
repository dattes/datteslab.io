---
title: Second life 
subtitle: Second life  databank analysis with DATTES
comments: false
---

# Outline
- [Information about the dataset](#information-about-the-dataset)
- [Information about the test](#information-about-the-test)
- [Data analysis with DATTES](#data-analysis-with-dattes)

> Note: DATTES 22.06 example. Do not work with DATTES 23.05.

# Information about the dataset
The "Second life battery databank" provides the data test results made by
[Marwan Hassini](https://mhassini.gitlab.io/) from [Licit-Eco7 laboratory](http://licit-lyon.eu/),
[University Gustave Eiffel](https://www.univ-gustave-eiffel.fr/)
during his thesis about second life batteries extracted from real electric vehicles.

This dataset have been used in two publications :
- [Second Life Batteries in a Mobile Charging Station : Model Based Performance Assessment](https://www.researchgate.net/publication/361309990_Second_Life_Batteries_in_a_Mobile_Charging_Station_Model_Based_Performance_Assessment)
- [Second Life Batteries in a Mobile Charging Station : Experimental Performance Assessment](https://data.univ-gustave-eiffel.fr/dataverse/second_life_batteries)

Dataset, test description and data reuse conditions can be found at [Univ Gustave Eiffel dataverse](https://data.univ-gustave-eiffel.fr/dataverse/second_life_batteries)


# Information about the test
The test is a characterization test performed with a
[Bitrode FTV250 battery cycler](https://www.bitrode.ru/wp-content/uploads/2019/12/FTV-Datasheet.pdf)
on second life [SAMSUNG SDI 94Ah cells](https://files.gwl.eu/inc/_doc/attach/StoItem/7213/30118_Introduction%20of%20SDI%20EV%2094Ah%20cell_V9-2.pdf).


# Data analysis with DATTES
In this example, it is assumed that DATTES is installed on your computer.
If it is not the case, please follow these instructions : [Getting started](/page/documentation/getting_started/).


1. First, DATTES must be initialized 
```
% Change the working folder to reach DATTES repository
cd DATTES_folder
% Initialize DATTES
initpath_dattes
```

2. Then, change the working folder to reach [second life batteries experimental data](https://data.univ-gustave-eiffel.fr/dataverse/second_life_batteries).

```
% Change the working folder to reach experimental data
 cd Second_life_battery_databank
```

3. Convert the Bitrode result files into .xml format
```
% Convert the Bitrode .csv result files in current folder ('.') into .xml format
bitrode_csv2xml('.')
```

4. Gather all xml files into a single variable
 ```
% Search all .xml files in current folder ('.')
XML_list=lsFiles('.','.xml') 
```

5. Load the configuration ('cvs' = configure + verbose + save)
```
[result]=dattes(XML_list,'cvs','cfg_SAMSUNG_94Ah')`
```

6. Plot configuration ('Gc' = Graphics + configuration):
```
[result]=dattes(XML_list,'Gc')
```
![Image](/page/examples/second_life_Gc.png)


7. Plot phases ('Gp' = Graphics + phases):

```
[result]=dattes(XML_list,'Gp')
```

![Image](/page/examples/second_life_Gp.png)

In this plot each phase is numbered and plotted with a different color.
DATTES analyzes the test and cut it into phases depending of the cycler working mode (CC, CV, rest, etc.).

8. Calculate capacity and soc ('CSvs' = Capacity + SoC + verbose + save) 
```
[result]=dattes(XML_list,'CSvs')
```
9. Plot capacity ('GC' = Graphics + Capacity):

```
[result]=dattes(XML_list,'GC')
```

![Image](/page/examples/second_life_GC.png)

This plot shows different capacity measurements found in this test versus current rate (1C, C/20; charge and discharge).
Positive values of current rate correspond to charging phases,
negative values of current rate correspond to discharging phases. 

9. Plot state of charge ('GS' = Graphics + Soc):

```
[result]=dattes(XML_list,'GS')
```

![Image](/page/examples/second_life_GS.png)
This plot shows depth of discharge (Ah) during test.
Red circles SoC100 points identified by DATTES (SoC reference).

11. Calculate the resistance ('Rvs' = Resistance + verbose + save):
```
[result]=dattes(XML_list,'Rvs')
```


12. Plot resistance ('GR' = Graphics + Resistance):
```
[result]=dattes(XML_list,'GR')
```
![Image](/page/examples/second_life_GR.png)

Left subplot shows resistance of different pulses versus depth of discharge (Ah).
Right subplot shows resistance of different pulses versus current rate (C).
