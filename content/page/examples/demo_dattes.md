---
title: DATTES demo 
subtitle:
comments: false
---

This example is included with DATTES since 23.05 release, it helps you to test your installation and to explore
main features of DATTES.
You can run this example by typing `demo_dattes`.

# Outline
- [Information about the dataset](#information-about-the-dataset)
- [Information about the test](#information-about-the-test)
- [Data analysis with DATTES](#data-analysis-with-dattes)

# Information about the dataset
This dataset contains a sample characterisation test of a second life battery cell performed at Gustave Eiffel University.
The data used in this demo can be downloaded following instructions of `demo_dattes` script.

More experimental data is available in the "Second life battery databank".
This databank provides the data test results made by
[Marwan Hassini](https://mhassini.gitlab.io/) from [Licit-Eco7 laboratory](http://licit-lyon.eu/),
[University Gustave Eiffel](https://www.univ-gustave-eiffel.fr/)
during his thesis about second life batteries extracted from real electric vehicles.

Data from this experiments have been used in two publications :
- [Second Life Batteries in a Mobile Charging Station : Model Based Performance Assessment](https://www.researchgate.net/publication/361309990_Second_Life_Batteries_in_a_Mobile_Charging_Station_Model_Based_Performance_Assessment)
- [Second Life Batteries in a Mobile Charging Station : Experimental Performance Assessment](https://data.univ-gustave-eiffel.fr/dataverse/second_life_batteries)

Dataset, test description and data reuse conditions can be found at [Univ Gustave Eiffel dataverse](https://data.univ-gustave-eiffel.fr/dataverse/second_life_batteries)

# Information about the test
The test is a characterization test performed with a
[Biologic BCS battery cycler](https://www.biologic.net/products/bcs-800/)
on second life [SAMSUNG SDI 94Ah cells](https://files.gwl.eu/inc/_doc/attach/StoItem/7213/30118_Introduction%20of%20SDI%20EV%2094Ah%20cell_V9-2.pdf).

# Data analysis with DATTES
To run this example you must :
1. Download and install DATTES
1. Run `initpath_dattes`
1. Download data required for this script (type `help demo_dattes` for more details).
1. Go to the folder containing **20230414_1501_BUGE382_M3_L1_caracini.zip**
1. Run `demo_dattes`

## dattes_import
The first step is to convert raw files (Biologic mpt files) to XML.

## dattes_structure
The second step is to structure information in the test.
First figure shows the voltage and current profiles during the test, colors are used for each cycler mode (rest, CC, CV, etc.).
Second figure shows numbers on each phase after segmentation based on cycler working mode.
![Image](/page/examples/demo1.jpg)
![Image](/page/examples/demo2.jpg)

## dattes_configure
The third step is to select phases for each analysis and to configure analysis methologies.
The following figure shows the configuration results:
![Image](/page/examples/demo3.jpg)

## dattes_analyse
After **dattes_import**, **dattes_struture** and **dattes_configure**, the **dattes_analyse** phase can be used for different data analysis:

- Capacity:
![Image](/page/examples/demo4.jpg)
- Resistance:
![Image](/page/examples/demo5.jpg)
- Impedance:
![Image](/page/examples/demo6.jpg)
![Image](/page/examples/demo7.jpg)
![Image](/page/examples/demo8.jpg)
- OCV by points (partial charges/discharges):
![Image](/page/examples/demo9.jpg)
- pseudo OCV:
![Image](/page/examples/demo10.jpg)
- ICA/DVA:
![Image](/page/examples/demo11.jpg)

