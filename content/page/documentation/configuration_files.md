---
title: Configure files for analysis
subtitle: 
comments: false
---

The section [**Import cycler files**](/page/documentation/import_files) have explained how to convert an experimental file from a proprietary format into a standard one.

The section [**Structure test files for the analysis**](/page/documentation/structure_files) have explained how to structure the experimental file to facilitate the analysis. 




This section describes how to configure preferences for the analysis. To do so, two tutorials are available, the first one is about the analysis methodology defintion thanks to a configuration file and the second one is about anomaly detection.
- [**Define analysis methodology**](/page/documentation/create_configuration)
- [**Process anomaly detection**](/page/documentation/anomaly_detection)
