---
title: Managing excel files in DATTES
subtitle: 
comments: false
---

DATTES aims to work in any OS (Windows, Linux, Mac) independently if you
are using MATLAB or GNU Octave.

Handling Excel files varies depending on the operating system and platform
(MATLAB, GNU Octave).
For this reason, in DATTES we have chosen to use external python-based tools
to export excel files to a uniform .csv format.
This tools are called **pet4dattes** (python external tools for DATTES) and they
are included in DATTES in folder `external_tools/pet4dattes`.

To use **pet4dattes** you need python in your computer and you also need to
install some requirements.

> Ask your system administrator to install python in your computer if needed.

## Installation using **pip**
You can use **pip** to install **pet4dattes** requirements in this way:

1. Open a terminal (command window)
2. Go to **pet4dattes** folder:
```
cd path_to_dattes/external_tools/pet4dattes
```
3. Run **pip** :
```
pip install -r requirements.txt
```
4. Test installation:
```
python3 ss2csv.py tests/*.*
```
This command will convert every `*.xls`, `*.xlsx` and `*.ods` in `tests`
subfolder to `*.csv` files (one file per sheet).

5. Delete `*.csv` files, e.g. in unix systems:
```
rm tests/*/*.csv
rmdir tests/*
```

> If you have any other python based software you should consider to create a
specific environment for each software to avoid conflict between requirements.
> 
> This can be done with **venv** or **conda** for example.

## Installation using **conda** and **pip**

1. Download and install anaconda or miniconda
    - e.g. for miniconda: https://docs.anaconda.com/free/miniconda/
2. Create an environment:
```
conda create -n dattes python
```
3. Activate environment:
```
conda activate dattes
```
4. Install requirements
```
pip install -r requirements.txt
```
5. Test installation as above

6. From now when you will be using **pet4dattes** from MATLAB you will need
to start MATLAB from this environment.
For this you need to launch MATLAB from the terminal as follows:
```
conda activate dattes
matlab
```