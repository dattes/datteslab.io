--- 
title : DATTES plot gallery
---
DATTES will plot your experimental and processed data if you run :
`[result, config, phases] = dattes_plot(xml_file,options)`

# Profiles plot
![image](/images/figure_Gx.png)
![image](/images/figure_Gx_I.png)

# Phases plot
![image](/images/figure_Gp.png)
![image](/images/figure_Gp_I.png)

# Configuration plot
![image](/images/figure_Gc.png)
![image](/images/figure_Gc_2.png)
![image](/images/figure_Gc_3.png)

# Capacity plot
![image](/images/figure_GC.png)

# State-Of-Charge plot
![image](/images/figure_GS.png)

# Resistance plot
![image](/images/figure_GR.png)

# Impedance plot
![image](/images/figure_GZ.png)

# Open Circuit Voltage plot
![image](/images/figure_GP.png)
![image](/images/figure_GO.png)

# Electrochemical Impedance Spectroscopy plot
![image](/images/figure_Ge.png)

# Incremental Capacity Analysis plot
![image](/images/figure_GI.png)
