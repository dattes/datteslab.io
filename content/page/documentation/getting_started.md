 ---
title: Getting started
subtitle: 
comments: false
---
This section describes how to install DATTES and how to start using it.

## Requirements
DATTES works with [**Matlab**](http://www.matlab.com/) and [**Octave**](https://www.gnu.org/software/octave). None Toolboxes are needed

## Installation
### User install
1. Download a [**release**](https://gitlab.com/dattes/dattes/-/releases) of DATTES (preferably the latest one)
2. Open Matlab or Octave and change the working folder to reach DATTES
3. Run `initpath_dattes.m`

>   This script adds all DATTES' code folders to your Path.

4. Run `demo_dattes.m`

>  This script helps you to get some experimental data and shows you the main features of DATTES.

>  This script can be also used to test your install, if no errors are found, DATTES is ready to use.

### Developer install
If you wish to contribute to DATTES, you should get the latest version from the Gitlab repository.
To do so, you must have **git** installed, see [**official site**](https://git-scm.com/downloads) for download and install intructions.

1. Clone the DATTES repository: `git clone https://gitlab.com/dattes/dattes.git`
2. Open Matlab or Octave and change the working folder to reach DATTES
3. Run `initpath_dattes.m`
4. Run `demo_dattes.m`


Before you start contributing to DATTES, please read the [**contributing guidelines.**](/page/contribute)

## First examples

You can learn how to use DATTES thanks to :

 -  [**Tutorials**](/page/documentation/tutorials) which describe step by step how to analyze a test thanks to DATTES
 -  [**Examples**](/page/examples/examples) of some publics datasets analysis thanks to DATTES

