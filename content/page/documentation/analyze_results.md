  ---
title: Analyze main features
subtitle: 
comments: false
---
The section [**Import cycler files**](/page/documentation/import_files) have explained how to convert an experimental file from a proprietary format into a standard one.

The section [**Structure test files for the analysis**](/page/documentation/structure_files) have explained how to structure the experimental file to facilitate the analysis. 

The section [**Configuration test files for the analysis**](/page/documentation/create_configuration) have explained how to configure the main features analysis.

This section describes how to process the analysis of energy storage systems main features. To do so, one tutorial per feature is available. 

- [**Capacity**](/page/documentation/analyze_capacity)
- [**State of charge**](/page/documentation/analyze_soc)
- [**Resistance**](/page/documentation/analyze_resistance)

- [**Impedance**](/page/documentation/analyze_soc)
- [**Open Circuit Voltage**](/page/documentation/analyze_ocv)

- [**Electrochemical Impedance Spectroscopy**](/page/documentation/analyze_eis)
- [**Incremental Capacity Analysis**](/page/documentation/analyze_ica)

