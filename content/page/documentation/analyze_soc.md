--- 
title : State-of-Charge
---
# Outline
- [**State-of-Charge definition**](#state-of-charge-definition)
- [**Analysis preparation**](#analysis-preparation)
- [**Anomaly detection**](#anomaly-detection)
- [**State-of-Charge analysis**](#state-of-charge-analysis)
- [**Result visualization**](#result-visualization)
- [**Methodology and hypothesis**](#methodology-and-hypothesis)
- [**Contribute to soc analysis**](#contribute-to-soc-analysis)

## State-of-Charge definition
State of charge (SoC) denotes the capacity of a cell that is currently available over the rated capacity. The value of the SOC varies between 0 for a fully discharged cell and 1 for a fully charged one.  An alternative form of the same measure is the depth of discharge (DoD), the inverse of SoC.

$$ SoC= {  1\over 3600Q } \int_0^t \mathrm{i(t)} \mathrm{d}t$$
$$ DoD= 1-SoC$$
with 
- SoC : the state of charge (-)
- Q : the rated capacity (Ah)
- i(t) : the current of charge or discharge (A)
- DoD : the depth of discharge (-)

## Analysis preparation
DATTES is called as follows : `[result]=dattes(XML_file,'action','configuration_file')`. 

Before any analysis, it is then necessary to create the XML and configuration files.

The section [Import cycler files](/page/documentation/import_files) to analysis explains how to create the XML file.

The section [Create a configuration file]()page/documentation/create_configuration  explains how to create a configuration file.

## Anomaly detection
State of charge is the result of a calculation based on current integration and rated capacity consequently it is indirectly affected by current measurement quality. 

To check if a state of charge calculation have run normally the action 'S' should be used :
 `[result] = dattes(XMLfile,'Svs');` and plotted :  `[result] = dattes(XMLfile,'GS');`

A normal capacity test should look like the following image :

![image](/images/normal_soc_test.png)

Anomaly can be easily identified if one of the following elements appear :
- SoC level over 100%
- SoC level under 0%
- Gaps in the SoC line

## State-of-Charge analysis

To analyze the state of charge, the action 'S' should be used :

`[result] = dattes(XMLfile,'Svs');`

The output are :

| Output structure | Field | Array | Unit |Description |
| :------ |:--- | :--- |:--- |:--- |
| result |soc| `dod_ah_ini` | Ah | Initial depth of discharge|
| result |soc| `soc_ini` | % | Initial state of charge|
| result | soc|`dod_ah_fin` | Ah | Final depth of discharge|
| result | soc|`soc_fin` | %| Final state of charge|


## Code for visualization
To visualize the state of charge,  the action 'GS' should be used :

`[result] = dattes(XMLfile,'GS');`

The graph should look like

![image](/images/figure_GS.png)




## Methodology and Hypothesis
### Method
State of charge calculation is made in the function `calcul_soc.m` :

```
SOC = 100*(1+Q/config.Capa);
DoDAh = -Q;
```
The capacity is determined in the function `calcul_amphour.m` :

```
Q = cumtrapz(t,I)/3600;
```


### Key parameters for the calculation
The key parameter for state of charge calculation is :
- **config.Capa**.

It is a parameter given by the user in the configuration file. Usually, it is the nominal capacity from datasheet which is considered .


### Assumptions and possible simplifications
A trapezoidal rule have been preferred for the calculation of Ah:
- [**Numerical integration methods**](https://en.wikipedia.org/wiki/Numerical_integration) 
- [**Trapezoidal rule**](https://en.wikipedia.org/wiki/Trapezoidal_rule)

## Contribute to soc analysis
A list of open issues related to state of charge calculation and visualization may be available [**here**](https://gitlab.com/dattes/dattes/-/issues).

