---
title: Import files for analysis
subtitle: 
comments: false
---

## Compatible battery cycler files
DATTES is able to analyse result files from the following battery cyclers :
- Arbin (.csv, .res, .xls)
- Biologic (.mpt)
- Bitrode (.csv)
- Digatron (.csv)
- Landt (.xls)
- Neware (.csv)

If your cycler is not in this list, we would be pleased to add it. 

Please send a request to dattes@univ-eiffel.fr or create an issue [**here**](https://gitlab.com/dattes/dattes/-/issues)

## Introduction

The first step of DATTES consist in calling the function `dattes_import` to convert the raw data from the cycler into a standard file format.
The chosen file format is XML following the VEHLIB XML structure.

>  **_Note_ : Why a .xml format ?**
DATTES processes data analysis over .xml files for two reasons.     
     First, [**Extend Markup Language (xml)**](https://en.wikipedia.org/wiki/XML) is a standard file format that is both human-readable and machine-readable.        
   Second, DATTES have been designed to provide experimental results inputs to the electric vehicle simulation tool [**VEHLIB**](https://gitlab.univ-eiffel.fr/eco7/vehlib). This software uses .xml files.


## Syntax

The general syntax of `dattes_import` is:

- `
dattes_import(srcdir,cycler,options,dstdir, file_ext)
`

with:

- srcdir [char]: source folder to search cycler files
         [cell]: file list of cycler files
- cycler [char]: cycler name
  - 'arbin_csv': search for arbin csv files
  - 'arbin_res': search for arbin res files
  - 'arbin_xls': search for arbin xls files
  - 'biologic': search for biologic mpt files
  - 'bitrode': search for bitrode csv files
  - 'digatron': search for digatron csv files
  - 'landt': search for neware xls files
  - 'neware': search for neware csv files
- options [char]:
  - 'v': verbose, tell what you do
  - 'f': force, export files even if xml files already exist
  - 'm': merge, merge files in each folder into one single xml (only
arbin_csv and biologic)
  (defaults: no verbose, no force)
- dstdir [char]: destination folder for xml files, if not given xml are
written in srcdir
- file_ext [char]: search for alternative extension

## Examples:
### Simplest example
Import files in srcdir and put XML files in the same folder:
- Arbin csv: `dattes_import(srcdir,'arbin_csv')`
- Arbin res: `dattes_import(srcdir,'arbin_res')`
- Arbin xls: `dattes_import(srcdir,'arbin_xls')`
- Biologic: `dattes_import(srcdir,'biologic')`
- Bitrode: `dattes_import(srcdir,'bitrode')`
- Digatron: `dattes_import(srcdir,'digatron')`
- Landt xls: `dattes_import(srcdir,'landt_xls')`
- Neware: `dattes_import(srcdir,'neware')`
>  Note (1): for Arbin res files, mdbtools must be installed in your computer.
> 
> See https://github.com/mdbtools/mdbtools .

>  Note (2): for Arbin or Landt xls files, you need **python** and **pet4dattes**.
> 
> See [**Managing Excel files**](/page/documentation/managing_excel_files)

### Execution options: verbose, merge, force
1. **Verbose** option, some information is returned to the standard output:
    - Arbin csv: `dattes_import(srcdir,'arbin_csv','v')`
    - Arbin res: `dattes_import(srcdir,'arbin_res','v')`
    - Arbin xls: `dattes_import(srcdir,'arbin_xls','v')`
    - Biologic: `dattes_import(srcdir,'biologic','v')`
    - Bitrode: `dattes_import(srcdir,'bitrode','v')`
    - Digatron: `dattes_import(srcdir,'digatron','v')`
    - Neware: `dattes_import(srcdir,'neware','v')`
1. **Merge** option, merge several files into one single XML (available only for 'arbin_csv' and 'biologic'):
    - Arbin csv: `dattes_import(srcdir,'arbin_csv','m')`
    - Biologic: `dattes_import(srcdir,'biologic','m')`
1. **Force** option, if XML file already exists `dattes_import` ignores it except force option is turned on:
    - Arbin csv: `dattes_import(srcdir,'arbin_csv','f')`
    - Arbin res: `dattes_import(srcdir,'arbin_res','f')`
    - Arbin xls: `dattes_import(srcdir,'arbin_xls','f')`
    - Biologic: `dattes_import(srcdir,'biologic','f')`
    - Bitrode: `dattes_import(srcdir,'bitrode','f')`
    - Digatron: `dattes_import(srcdir,'digatron','f')`
    - Neware: `dattes_import(srcdir,'neware','f')`
> Note: options 'v', 'm' and 'f' may be combined, e.g.: `dattes_import(srcdir,'biologic','vmf')`

### Write XML files in alternate folder
Import files in **srcdir** and put XML files in the other folder different than srcdir (**dstdir**):
- Arbin csv: `dattes_import(srcdir,'arbin_csv',options,dstdir)`
- Arbin res: `dattes_import(srcdir,'arbin_res',options,dstdir)`
- Arbin xls: `dattes_import(srcdir,'arbin_xls',options,dstdir)`
- Biologic: `dattes_import(srcdir,'biologic',options,dstdir)`
- Bitrode: `dattes_import(srcdir,'bitrode',options,dstdir)`
- Digatron: `dattes_import(srcdir,'digatron',options,dstdir)`
- Neware: `dattes_import(srcdir,'neware',options,dstdir)`

### Import a list of files:
If you have a list of files (cell of pathnames), you can use it instead searching for file in srcdir, e.g.:
- `csv_list = {'folder1/test_capacity.csv','folder2/test_impedance.csv'};`
- `dattes_import(csv_list,cycler,options,dstdir);`

>  Note: **options** and **dstdir** are optional

### Working with alternate file extensions:
Sometimes, input files have differetn extensions. For example biologic files sometimes are **.txt** instead **.mpt**:
- `dattes_import(srcdir,'biologic',options,dstdir,'.txt')`

>  Note: **options** and **dstdir** are optional, if you want the defults, simply set them to empty chars:
`dattes_import(srcdir,'biologic','','','.txt')`

### Getting the list of XML files:
`dattes_import` returns three file lists (cells of pathnames):

- `[xml_files, failed_filelist,ignored_list] = dattes_import(...);`
    - **xml_files**: succesfuly imported XML files
    - **failed_filelist**: input files that did not work
    - **ignored_list**: ignored input files (corresponding XML files already exist)

