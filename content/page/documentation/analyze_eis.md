 --- 
title : Electrochemical Impedance Spectroscopy
---
# Outline
- [**Electrochemical Impedance Spectroscopy definition**](#eis-definition)
- [**Analysis preparation**](#analysis-preparation)
- [**Anomaly detection**](#anomaly-detection)
- [**EIS analysis**](#eis-analysis)
- [**Result visualization**](#result-visualization)
- [**Methodology and hypothesis**](#methodology-and-hypothesis)
- [**Contribute to eis analysis**](#contribute-to-eis-analysis)

## Electrochemical Impedance Spectroscopy analysis definition
Electrochemical Impedance Spectroscopy (EIS) is an electrochemical technique to measure the impedance of a system in dependence of the AC potentials frequency.

## Analysis preparation
DATTES is called as follows : `[result]=dattes(XML_file,'action','configuration_file')`. 
Before any analysis, it is then necessary to create the XML and configuration files.

The section [**Import cycler files**](/page/documentation/import_files) explains how to create the XML file. 

The section [**Create a configuration file**](/page/documentation/create_configuration) explains how to create a configuration file.

## Anomaly detection



##Electrochemical Impedance Spectroscopy analysis

To analyze the electrochemical impedance spectroscopy tests, the action 'c' should be used :

`[result] = dattes(XML_file,'cvs','cfg_file');`

The outputs will be :

| Output field | Array | Unit |Description |
| :------ |:--- | :--- |:--- |
| result |  eis|  `t`  |  s |  Measurement instant of the point |
| result | eis |  `U` | V | Measured voltage |
| result | eis |   `I` | A | Measured current |
| result | eis |   `m` | | Test mode (1=CC, 2=CV, 3=rest, 4=EIS, 5=profile)|
| result | eis |  `ReZ` | Ohm | Real part of the impedance|
| result | eis |  `ImZ` | Ohm | Imaginary part of the impedance|
| result | eis |  `f` | Hz | Measurement frequency |

## Code for vizualization
To visualize the electrochemical impedance spectroscopy, the action 'Ge' should be used :

`[result] = dattes(XMLfile,'Ge');`

The graph should look like

![image](/images/figure_Ge.png)




## Methodology and Hypothesis
### Method
EIS is processed in function `extract_eis` from `extract_profiles`

### Key quantities for the calculation
The two key parameters for the identification of the eis are :
- **config.test.Uname**, 
- **config.test.Tname**.



### Assumptions and possible simplifications
No major assumptions or simplifications have been made

## Contribute to eis analysis
A list of open issues related to eis calculation and visualization may be available [**here**](https://gitlab.com/dattes/dattes/-/issues/?search=eis&sort=created_date&state=opened&first_page_size=20)



