---
title: Installation
subtitle: Installing DATTES 
comments: false
---
# Requirements
DATTES works with Matlab and Octave. None Toolboxes are needed

# Installation
The sources for `DATTES` can be downloaded from the [Gitlab repository.](https://gitlab.com/dattes/dattes)

1. The different releases of DATTES are available [here](https://gitlab.com/dattes/dattes/-/releases)
2. Download the latest release
3. Open Matlab or Octave and change the working folder to reach DATTES
4. Open and run `initpath_dattes.m`
5. DATTES is ready to use

You can learn how to us DATTES thanks to :

 - Tutorials :[Link](/page/documentation/tutorials/)
 - Examples : [Link](/page/examples/examples)

