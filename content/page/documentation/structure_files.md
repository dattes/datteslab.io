---
title: Structure files for analysis
subtitle: 
comments: false
---

The section [**Import cycler files**](/page/documentation/import_files) have explained how to create the XML file. 


## Introduction
This section describes the econd step of the [DATTES workflow](/page/documentation/dattes_workflow): how to structure experimental files for analysis.
This step is performed by calling the function `dattes_structure`.

With this function experimental data is converted to [DATTES structure](/page/documentation/dattes_structure).
This structure contains
  - main battery variables: date/time, voltage, current
  - other variables: SoC (state of charge), cycler mode (rest, CC,CV, etc.)
  - metadata if **.meta** files are found
  - experiment decomposition into phases (rest, CC, CV, etc.) and general statistics of each phase

 > For more information abobut how metadata is managed in DATTES see [this page](/page/documentation/write_metadata).


## Syntax

The general syntax of `dattes_structure` is:

- Read a file: `
result = dattes_structure(file_in, options, destination_folder,read_mode)
`
- Read files in a folder: `
result = dattes_structure(source_folder, options, destination_folder, read_mode)
`

with:

- file_in [char]: pathname to the file where extract information  (xml, json or csv file)
- source_folder [char]: alternatively to file_in, read all files found in this folder 
- options [char]:
  - 'v': verbose, tell what you do
  - 's': save, store result in a mat file (file_out)
  - 'f': force, read file_in even if file_out exists
  - 'u': update, read file_in if it is more recent than file_out
  - 'S': recalculate SoC
  - 'm': redetect mode (rest, CC, CV, etc.)
- destination_folder [char]: destination folder for mat files, if not given mat are
written  same folder than file_in
- read_mode [char]:
  - 'xml' search/read xml files (default)
  - 'json' search/read json files
  - 'csv' search/read csv files
  - 'mat' search/read mat files

The returned value `result` is a [DATTES structure](/page/documentation/dattes_structure) (file_in) or a cell containing DATTES structures (source_folder).

## Examples:
### Simplest example
- Read a file: `result = dattes_structure(file_in)`
- Read all xml file in a folder: `result = dattes_structure(source_folder)`


### Execution options: verbose, save, update, force
1. **Verbose** option, some information is returned to the standard output:
    - Read a file: `result = dattes_structure(file_in,'v')`
    - Read all xml file in a folder: `result = dattes_structure(source_folder,'v')`
1. **Save** option, store result structure(s) in mat file(s):
    - Read a file: `result = dattes_structure(file_in,'s')`
    - Read all xml file in a folder: `result = dattes_structure(source_folder,'s')`
1. **Force** option, if mat file already exists `dattes_structure` read mat file instead xml (faster) except if force option is turned on:
    - Read a file: `result = dattes_structure(file_in,'f')`
    - Read all xml file in a folder: `result = dattes_structure(source_folder,'f')`
1. **Update** option, ignore xml if mat file exist except if xml is newer than file:
    - Read a file: `result = dattes_structure(file_in,'u')`
    - Read all xml file in a folder: `result = dattes_structure(source_folder,'u')`
> Note: options 'v', 'm' and 'f' (or 'u') may be combined, e.g.: `dattes_structure(source_folder,'vsu')`

### Other execution options: recalculate SoC, redetect mode
1. Recalculate **SoC** option:
    - Read a file: `result = dattes_structure(file_in,'S')`
    - Read all xml file in a folder: `result = dattes_structure(source_folder,'S')`
1. Redetect **mode** option:
    - Read a file: `result = dattes_structure(file_in,'m')`
    - Read all xml file in a folder: `result = dattes_structure(source_folder,'m')`

