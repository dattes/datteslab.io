---
title: Create a configuration file
subtitle: 
comments: false
---

## What is a configuration file ?
The configuration file contains the following informations :
- Key parameters of the tested cell
- Informations about the test made
- Informations about the way the analysis should be processed
- Informations about the way the graph should be plotted
- Metadata about the test

A list of existing configurations files is available locally in the `.../dattes/configs/` folder or [**remotely here**](https://gitlab.com/dattes/dattes/-/tree/main/configs)

Each configuration file is named as follows :`cfg_Cellbrand_Cellmodel_CapacityAh.m`

The following table provides some examples of configurations files names.


| Cell Brand | Cell Capacity (Ah) | Configuration file |
| :------ |:--- | :--- |
| SAMSUNG | 94 | `cfg_SAMSUNG_94Ah` |
| Kokam | 0.35 | `cfg_Kokam_0Ah35` |



Each configuration file should be structured as this example :
```
function config = cfg_SAMSUNG_94Ah

%%%%% 1-Specificities of the tested cell %%%%%

%values for this cell:
config.test.max_voltage = 4.15;
config.test.min_voltage = 2.7;
config.test.capacity = 94;

%%%%% 2-Load default values %%%%%
config = cfg_default(config);




%%%%% 3-Specificities of the test carried out %%%%%

config.test.Uname = 'U1';%nom pour la variable tension (Bitrode AuxVoltage1 = U1)
config.test.Tname = 'T1';%nom pour la variable temperature

 
% %ident_R
config.resistance.delta_time=[0 4 9 29 59]; %Instant du pulse auquel on mesure R (par défaut 0 secondes)
config.resistance.pulse_min_duration = 9;%duree min d'un pulse pour resistance
config.resistance.rest_min_duration =9;%duree min repos avant

% ident_RC
% config.maximal_duration_pulse_measurement_R = 40; % Durée maximale pour l'identification d'un RC
config.impedance.initial_params = [1e-3, 150, 1e-2, 400];% ident_rrc R1ini, C1ini, R2ini, C2ini
config.impedance.ident_fcn = @ident_rrc;% use ident_rrc

config.Rmin=1e-4;
config.Cmin=50;
config.Rmax=5e-2;
config.Cmax=800000;



% %ident_CPE2
% config.tminW = 59;%duree min d'un pulse pour diffusion
% config.tminWr = 59;%duree min repos avant
% config.CPEafixe = 0.5;%ident CPE2: valeur d'alpha du CPE (si = zero, alpha non fixe).
% 
% %ident_OCVr
% config.tminOCVr = 10;%duree min repos pour prise de point OCV
% config.dodmaxOCVr = 0.99;%delta soc max prise de point OCV en p.u. (0.5 = 50% soc)
% config.dodminOCVr = 0.01;%delta soc min prise de point OCV en p.u. (0.01 = 1% soc)
% 
% %pseudoOCV
config.pseudo_ocv.max_crate = 0.08;%regime max pour pseudoOCV (C/20) 
config.pseudo_ocv.min_crate = 0.03;%regime max pour pseudoOCV (C/20)
config.pseudo_ocv.capacity_resolution = config.test.capacity/100;%dQ pour pseudoOCV


%%Graphiques
config.langue_plot="EN";
config.langue_plot="FR";
config.DoD_or_SoC="DoD";
config.over100_or_over1_or_Ah="over100";
config.time_scale="h";
config.reference_capacity="nominal";

end

```

The first section of the configuration file contains informations regarding the tested cell. Three information are needed.

| Structure field |  Array | Unit |Description |
| :------ |:--- | :--- |:--- |
| test | `max_voltage` | V | Cell maximum voltage|
| test | `min_voltage` | V |  Cell minimal voltage|
| test | `capacity` | Ah |  Cell nominal capacity|


The second section is simply a call of the default configuration. This ensure that all fields of the configuration file exist.


The third section contains informations about the test made. It guides the data analysis for each specific test.


The following tables describes the different configuration arrays for each action.

For **'S' (SoC) action**, the array is :

| Structure field | Array | Unit |Description |
| :------ |:--- | :--- |:--- |
| soc | `crate_cv_end` |  | Current rate threshold for constant voltage phase (Default value C/20) |

For **'R' (Resistance) action**, the arrays are :

| Structure field | Array | Unit |Description |
| :------ |:--- | :--- |:--- |
| resistance | `delta_time` | s |  Resistance measurement instant |
| resistance | `pulse_min_duration` | s | Minimal duration of a pulse |
| resistance | `pulse_max_duration` | s | Maximal duration of a pulse |
| resistance | `rest_min_duration` | s | Minimal duration of a rest before a pulse|

For **'Z' (impedance) action**, the arrays are :

| Structure field | Array | Unit |Description |
| :------ |:--- | :--- |:--- |
| impedance | `ident_fcn` | - | Select @ident_cpe or @ident_rrc|
| impedance | `pulse_min_duration` | s | Minimal duration of a pulse |
| impedance | `pulse_max_duration` |  s | Maximal duration of a pulse |
| impedance | `rest_min_duration` | s | Minimal duration of a rest before a pulse|
| impedance | `fixed_params` |  | Array containing fixed values of model parameters |
| impedance | `initial_params` |  |Array containing initial values of model parameters |

For **'O' (open circuit voltage by points) action**, the arrays are :

| Structure field | Array | Unit |Description |
| :------ |:--- | :--- |:--- |
| ocv_points | `rest_min_duration` | s | Minimal duration for a constant current phase to be used for OCV measurement |
| ocv_points| `max_delta_dod_ah` |  1 |  Maximal dod variation to be taken into account for OCV measurement|
| ocv_points| `min_delta_dod_ah` | 1 |  Minimal dod variation to be taken into account for OCV measurement|


For **'I' (incremental capacity analysis) action**, the arrays are :

| Structure field | Array | Unit |Description |
| :------ |:--- | :--- |:--- |
| ica | `capacity_resolution` | Ah | Capacity resolution|
| ica | `voltage_resolution` | V | Voltage resolution|
| ica | `max_crate` | - | Maximal current rate for ICA |
| ica | `filter_type` |  | Filter type (N: no filter,G: gaussian filter,M: mean filter,B: butter filter) |
| ica | `filter_order` |  | Filter order (for gaussian) |
| ica | `filter_cut` |  |  Filter cutting frequency (for gaussian) |


For **'P' (pseudo open circuit voltage) action**, the arrays are :

| Structure field | Array | Unit |Description |
| :------ |:--- | :--- |:--- |
| pseudo_ocv | `max_crate` |  | Maximal current rate for pseudoOCV|
| pseudo_ocv | `min_crate` |  | Minimal current rate for pseudoOCV|
| pseudo_ocv | `capacity_resolution` |  Ah | Capacity resolution|




For **'G' (visualisation of the analysis result) action**, the arrays are :

| Structure field | Array | Unit |Description |
| :------ |:--- | :--- |:--- | 
| visualization | `GdDureeMin` | s | Minimal duration to plot a phase ('Gp')|
| visualization | `GdmaxPhases` | s | Maximal duration to plot a phase ('Gp')|



The configuration file of the cell SAMSUNG SDI 94Ah is given as an example, most of the configurations parameters should be adapted to your cell and your test.



---
>  **_Note_ : Recommandation for configuration files**

We recommand users to define a configuration file for each frequent test, for example :
- `cfg_SAMSUNG_94Ah_RPT.m`
- `cfg_SAMSUNG_94Ah_cycling.m`
- `cfg_SAMSUNG_94Ah_characterization_for_modeling.m.`

---
