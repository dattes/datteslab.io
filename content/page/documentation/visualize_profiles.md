--- 
title : Visualize profiles
---
# Outline
- [**Code for analysis**](#code-for-analysis)
- [**Code for visualization**](#code-for-visualization)
- [**Methodology and hypothesis**](#methodology-and-hypothesis)
- [**Contribute to profiles analysis**](#contribute-to-profiles-analysis)


## Code for analysis

To analyze the test profiles, the action 'c' should be used :

`[result] = dattes(XML_file,'cvs','cfg_file');`

## Code for vizualization
To visualize the test profiles, the action 'Gx' should be used :

`[result] = dattes(XML_file,'Gx','cfg_file');`

The graph should look like

![image](/images/figure_Gx.png)




## Methodology and Hypothesis
### Method
The profile analysis is done systematically. It is one of the first step in the analysis of data.
`[t,U,I,m,DoDAh,SOC,T, eis, err] = extract_profiles(thisXML,options,config) ` is the function responsible for the conversion of the XML file into profiles.

```
extract_profiles extract important variables from a battery test bench file.

Methodology
1. Read a .xml file (Biologic,Arbin, Bitrode...), if a dattes results file exists this latter will be read (faster) 
2. Extract important vectors: t,U,I,m,DoDAh,SOC,T
3. Save the important vectors in a dattes results file (if 's' in options)

Usage
 [t,U,I,m,DoDAh,SOC,T, err] = extract_profiles(thisXML): normal operation,
 
Error codes
err = 0: OK
err = -1: thisXML file does not exist
err = -2: dattes' result file is wrong
err = -3: some vectors are missing (t,U,I,m)

```

### Key quantities for the calculation
There are no calculations made. The function `extract_profiles` used the data from the XML file which creation is explained in the tutorial [**Import cycler files**](/page/documentation/import_files/)


### Assumptions and possible simplifications
No major assumptions or simplifications have been made

## Contribute to profiles analysis
A list of open issues related to profiles analysis and visualization may be available [**here**](https://gitlab.com/dattes/dattes/-/issues/?search=profiles&sort=created_date&state=opened&first_page_size=20).

 
